# jsoneditor-validator

[JSON Editor](https://github.com/json-editor/json-editor) validators

## Installation

```javascript
import { JSONEditor } from "@json-editor/json-editor/src/core";
import Validators from "@gaia/jsoneditor-validator";

JSONEditor.defaults.custom_validators = Validators;
```
