import dateValidator from "./date";
import datetimeValidator from "./datetime";
import emailValidator from "./email";
import propertyRequiredValidator from "./property-required";

export default [
    dateValidator,
    datetimeValidator,
    emailValidator,
    propertyRequiredValidator
];