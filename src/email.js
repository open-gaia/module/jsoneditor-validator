import { defaults } from "@json-editor/json-editor/src/defaults";
import isEmail from "validator/es/lib/isEmail";

/**
 * @param schema
 * @param value
 * @param path
 * @return {[]}
 */
export default (schema, value, path) => {

    let errors = [];
    if(schema.format !== "email") {
        return errors;
    }

    if(!isEmail(value)) {
        errors.push({
            path: path,
            property: 'format',
            message: defaults.translate('error_email')
        });
    }

    return errors;
};
