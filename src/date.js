import { defaults } from "@json-editor/json-editor/src/defaults";
import isISO8601 from "validator/es/lib/isISO8601";

/**
 * @param schema
 * @param value
 * @param path
 * @return {[]}
 */
export default (schema, value, path) => {

    let errors = [];
    if(schema.format !== "date") {
        return errors;
    }

    if(!isISO8601(value)) {
        errors.push({
            path: path,
            property: 'format',
            message: defaults.translate('error_date_iso8601')
        });
    }

    return errors;
};
