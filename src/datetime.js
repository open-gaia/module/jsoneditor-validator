import { defaults } from "@json-editor/json-editor/src/defaults";
import isRFC3339 from "validator/es/lib/isRFC3339";

/**
 * @param schema
 * @param value
 * @param path
 * @return {[]}
 */
export default (schema, value, path) => {

    let errors = [];
    if(schema.format !== "date-time") {
        return errors;
    }

    if(!isRFC3339(value)) {
        errors.push({
            path: path,
            property: 'format',
            message: defaults.translate('error_datetime_rfc3339')
        });
    }

    return errors;
};
