import { defaults } from "@json-editor/json-editor/src/defaults";

/**
 * @param schema
 * @param value
 * @param path
 * @return {[]}
 */
export default (schema, value, path) => {

    let errors = [];
    if(!Array.isArray(schema.required)) {
        return errors;
    }

    for (const property in value) {
        if(undefined === value[property]) {
            errors.push({
                path: path + '.' + property,
                property: 'format',
                message: defaults.translate('error_property_required')
            });
        }
    }

    return errors;
};
